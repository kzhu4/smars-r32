SMARS is a robot platform. https://www.thingiverse.com/thing:2662828

![Smars Robot](docs/i/smarsiso_thumb_small.jpg)

We're using SMARS to bridge the gap between swarms simulations and experimentation in research.
To this end, we've made only one modification to the SMARS platform:
We've swapped the Arduino Uno inside for a Wemos D1-R32, an ESP-32 based board
that's shaped like an Arduino Uno.
We did this for easy WiFi connectivity for syncing experiments.
We're also using a WCMCU-L298P motor driver shield.

We've also written a set of drivers to make working with the modified platform
more convenient.

A MicroPython driver for this shield can be found in `/mpy/`

We also have a tool for making working with MicroPython on ESP-32 boards easier:
https://gitlab.orc.gmu.edu/kzhu4/wtf-boom

# Getting Started

First, either clone or download this repository.
We recommend using the Boom tool mentioned above to get started. Otherwise, you'll have to build your own toolchain.
Download the boom.exe file from the link above, and place it in the root folder of this project, alongside the included mpycfile.
To use Boom, you'll need some prerequisites. Learn how to setup Boom's dependencies at the link above.
Once you have Boom ready, download the appropriate MicroPython firmware.
https://micropython.org/download/esp32/
We recommend the latest release unless otherwise stated.
Place the .bin file alongside boom.exe and the mpycfile.
If needed, modify the first line in your mpycfile to match the filename of the firmware file you downloaded. Don't forget to begin this line with the `@` symbol to denote that this is a firmware file.
Finally, connect the board via USB to your computer, open Boom and select the correct port, and `Erase Flash`, `Flash MicroPython`, and `Compile & Upload` in that order.

Note: You may need drivers for your computer to talk to the board. Try searching for the name of the serial interface IC (i.e. CH340, CP2104, ftdi232) to find drivers.
