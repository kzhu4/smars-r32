# This file is executed on every boot (including wake-boot from deepsleep)
# import esp
# esp.osdebug(None)
# import webrepl
# webrepl.start()

from machine import Pin

for p in (5, 23, 17):
    Pin(p).off()


def reload(mod):
    """
    Reload an imported module.

    You can only import a file once.
    Use this to run it a second time (or more)
    """
    import gc
    from sys import modules
    mod_name = mod.__name__
    del modules[mod_name]
    gc.collect()
    return __import__(mod_name)


def unload(mod):
    """Unload an imported module."""
    import gc
    from sys import modules
    mod_name = mod.__name__
    del modules[mod_name]
    gc.collect()


from wcmcu_l298p import *

# if True:
#     import az
#     az.main()
