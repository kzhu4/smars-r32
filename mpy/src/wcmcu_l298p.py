'''
Drivers for a Wemos D1 R32 + WCMCU_L298p shield.

This library provides easy access to the functions specific to
a Fundumoto WCMCU_L298P Uno shield on a Wemos D1 R32. In particular,
H-bridge channels, ESP32 NVS, servo, and ping objects have been pre-defined.

Arduino Uno pin number aliases are also available for convenience.

'''

from machine import Pin, PWM
import time
import machine
from errno import ETIMEDOUT
# import statistics_tools as st
import esp32

BOARD_VERSION = "E_GALAXY 2014/APRIL/12"

# ESP32 Non-volatile storage namespace.
# Provides EEPROM-like functionality.
nvs = esp32.NVS("r32")

# i2c = machine.I2C(1, scl=Pin(26), sda=Pin(25))

#
# Pin & address definitions
#

# Arduino to ESP pin numbering
D13 = 18  # hbridge inpt 3
D12 = 19  # hbridge inpt 1
D11 = 23  # hbridge enbl B
D10 = 5  # hbridge enbl A
D9 = SERVO = 13  # servo pwm
D8 = PING_ECHO = 12  # ping R
D7 = PING_TRIG = 14  # ping T
D6 = 27
D5 = 16
D4 = BUZZER = 17  # buzzer
D3 = 25
D2 = 26  # pin 2 I/O
D1 = TXD0 = 1  # TXD0
D0 = TXD0 = 3  # RXD0
A5 = 39
A4 = 36
A3 = 34
A2 = 35
A1 = 4
A0 = 2


#
# region: st  ---
# From statistics_tools.py
#
def fmap(x, in_min, in_max, out_min, out_max):
    """
    Given a set of ranges, remap x to a new set of ranges.

    Converted to Python from Arduino's Map() function.
    """
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


def imap(*args, **kwargs) -> int:
    """Given a set of ranges, remap x to a new set of ranges."""
    return int(fmap(*args, **kwargs))


def constrain(x, in_min, in_max):
    """Constrain input value between in_min and in_max."""
    return min(in_max, max(in_min, x))

# endregion: st  ---


class Servo(PWM):
    def __init__(self, pin, min_us, max_us, center_us=None, bidirectional=True, invert=False, freq=50):
        super().__init__(pin)
        super().init(freq=freq, duty=0)
        self.set_bounds(min_us, max_us, center_us, bidirectional, invert)

    @property
    def _a_us(self):
        return self.min_us if self.invert else self.max_us

    @property
    def _b_us(self):
        return self.max_us if self.invert else self.min_us

    def set_bounds(self, min_us, max_us, center_us=None, bidirectional=True, invert=False):
        # separate to allow for users to change pwm bounds easily later
        self.invert = invert
        self.min_us = min_us
        self.max_us = max_us
        # center_ms is halfway between min and max by default
        self.center_us = center_us if center_us else (min_us + max_us) / 2
        if center_us:
            raise NotImplementedError
        self.theta_a = -90 if bidirectional else 0
        self.theta_b = 90 if bidirectional else 180

    def angle(self, angle):
        duty = imap(angle, self.theta_a, self.theta_b, self._a_us, self._b_us)
        super().duty_ns(duty * 1000)

    def i10(self, value):
        duty = imap(value, -511, 511, self._a_us, self._b_us)
        super().duty_ns(duty * 1000)


class Motor(PWM):
    def __init__(self, pin_dir, pin_pwm, invert=False, freq=50):
        super().__init__(pin_pwm)
        super().init(freq=freq, duty=0)
        self.pin_dir = pin_dir
        self.pin_pwm = pin_pwm
        if invert:
            self.pin_dir.init(Pin.OUT, value=1)
            raise NotImplementedError
        else:
            self.pin_dir.init(Pin.OUT, value=0)

    @property
    def dir(self):
        return 1 if self.pin_dir.value else -1

    @dir.setter
    def dir(self, x):
        if isinstance(x, str):
            self.pin_dir.off() if 'forward' in x.lower() else self.pin_dir.on()
        else:
            self.pin_dir.on() if not x or x < 0 else self.pin_dir.off()

    def angle(self, angle):
        if angle is None:
            return fmap(super().duty_u16(), 0, 65535, 0, 90) * self.dir
        duty = constrain(imap(abs(angle), 0, 90, 0, 65535), 0, 65535)
        super().duty_u16(duty)
        self.pin_dir.on() if angle < 0 else self.pin_dir.off()

    def i10(self, value):
        if value is None:
            return fmap(super().duty_u16(), 0, 65535, 0, 511) * self.dir
        duty = constrain(imap(abs(value), 0, 511, 0, 65535), 0, 65535)
        super().duty_u16(duty)
        self.pin_dir.on() if value < 0 else self.pin_dir.off()


# H-Bridge output objects
m0 = mA = Motor(Pin(D12), Pin(D10))
m1 = mB = Motor(Pin(D13), Pin(D11))
m = [m0, m1]
# Servo output object
s0 = Servo(Pin(SERVO), 500, 2500)
s = [s0]


class HCSR04:
    # speed of sound in air at 20C is 343 m/s
    def __init__(self, trig, echo, timeout_us=None):
        self.trig = trig
        self.echo = echo
        self.timeout_us = timeout_us
        if timeout_us is None:
            self.timeout_us = 8 * 1_000_000 // 343  # limit to max distance of ~4m
        self.trig.init(Pin.OUT, value=0)
        self.echo.init(Pin.IN, pull=None, value=0)

    def ping_us(self, timeout_us=None):
        timeout_us = self.timeout_us if timeout_us is None else timeout_us
        self.trig.off()
        time.sleep_us(10)
        self.trig.on()
        time.sleep_us(10)
        self.trig.off()
        us = machine.time_pulse_us(self.echo, 1, timeout_us)
        self.trig.off()
        if us == -1:
            us = float('inf')
        if us == -2:
            raise OSError(ETIMEDOUT, "Timed out waiting for echo initialization signal.")
        return us

    def distance_m(self, timeout_us=None):
        us = self.ping_us(timeout_us)
        return us / 2 * 343 / 1e6  # convert to meters

    def distance_mm(self, timeout_us=None):
        us = self.ping_us(timeout_us)
        # speed of sound in air at 20C is 1/2.91545 mm/us
        # mm = us / 2.91545 / 2
        return us * 1000 / 5831


# Ping sensor object (HC-SR04 driver)
p0 = HCSR04(Pin(PING_TRIG), Pin(PING_ECHO))

#
# Misc modules
#
buzzer = Pin(BUZZER, Pin.OUT)
